﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace Downloader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebBrowser web = new WebBrowser();
            web.Navigate(new Uri("http://www.bbc.co.uk"));
            web.ScriptErrorsSuppressed = true;

            List<Image> images = new List<Image>();
            web.DocumentCompleted += (s, ea) =>
            {
                WebBrowser webCompleted = (WebBrowser)s;
                foreach (HtmlElement image in web.Document.Images)
                {
                    
                    dynamic htmlImage = image.DomElement;
                    images.Add(new Image
                    {
                        Name    = htmlImage.nameProp,
                        Url     = htmlImage.href,
                        Height  = image.ClientRectangle.Height,
                        Width   = image.ClientRectangle.Width
                    });
                }

                DownloadImages(images);
                MessageBox.Show("Downloaded Images");
            };

       }

        private void DownloadImages(List<Image> images)
        {
            WebClient wc = new WebClient();
            foreach (Image image in images)
            {
                //TODO Height and Width might be invalid. So files needs to be download to temporary location and next we can check size of files and move them to appropriate location
                string path = PrepareDirectory(@"C:\Download", image.Height + "x" + image.Width);

                try
                {
                    wc.DownloadFile(image.Url, path + image.Name);
                }
                catch (Exception)
                {
                    MessageBox.Show(string.Format("I'm sorry we had an error processing image {0}. Please download image manualy to directory {1}", image.Url, path));
                }
            }
        }

        private string PrepareDirectory(string currentPath, string directoryName)
        {
            if (currentPath.LastOrDefault() != '\\') currentPath += "\\";
            string newPath = currentPath + directoryName;
            if (!Directory.Exists(newPath)) Directory.CreateDirectory(newPath);
            return newPath + "\\";
        }
    }
}
